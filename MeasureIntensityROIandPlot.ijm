/* Oct2023
 *  Elvire Guiot 
 *  guiote@igbmc.fr
 *  
  Macro pour calcul des variations d'intensité au cours d'un timelapse
 
	La macro enregistre automatiquement dans le dossier qui contient les données sources des fichiers.txt qui contiennent 
	respectivement les valeurs des temps réels des images, les valeurs d'intensité moyennes, et normalisées à la ligne de base
	La macro enregistre aussi le ROIset (ROI tracée pour la quantification ) et un jpeg du tracé des ratios afin d'avoir un visuel sur la manip,
	le tout avec comme nom de base celui de la série d'image brute

 */
 

var	idImage;


	//___________________________________________________________________________________________//	
	

idImage = getImageID();	
selectImage(idImage);	

//rename("ImageBrute");	

run("ROI Manager...");
roiManager("Show All");
selectImage(idImage);

run("Grays");
//run("In [+]");
waitForUser("Draw the ROI and press T to save in the RoiManager");
roiManager("Show All");

run("Set Measurements...", "mean redirect=None decimal=4");

setBatchMode(true);

// mesure de l'intensité moyenne 
selectImage(idImage);
roiManager("Multi Measure");
selectWindow("Results");




//color ROI 

color = newArray ( "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray","black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","gray", "black", "blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow","blue", "cyan", "darkGray", "green", "lightGray", "magenta", "orange", "pink", "red", "white", "yellow");
count = roiManager('count');

for (i = 0; i < count ; i++) {
		roiManager("select", i);
		roiManager("Set Color", color[i]);
		roiManager("Set Line Width", 2);
		roiManager('update');
}


//plot des int au cours du temps
Plot.create("Intensite", "x", "Mean1");
Plot.add("line", Table.getColumn("Mean1", "Results"));
Plot.setColor("black");
minRatio = 1;
MaxRatio = 0;


for (i = 2; i <= roiManager("count") ; i++) {
    ColumnName = "Mean" + toString(i);
    test = getResultString(ColumnName, 1);
    max_column=0;
	min_column=0;
    if (test != "null") {
    	Plot.add("line", Table.getColumn(ColumnName, "Results"));
		colorLine = color[i-1];
		Plot.setColor(colorLine);
		
				//Max value in "ColumnName" column					/* recheche des min et max values pour l'échelle du graph*/
				for (a=0; a<nResults(); a++) {
		  		  if (getResult(ColumnName,a)>max_column)
				    {
		  		   max_column = getResult(ColumnName,a);
				    	}
				    	else{};
				}
				
				//Min value in "ColumnName" column (note: requires max value)
				min_column=max_column;
				for (a=0; a<nResults(); a++) {
				    if (getResult(ColumnName,a)<min_column)
				    {
				     min_column = getResult(ColumnName,a);
				    	}
				    	else{};
				}
		
    	} 
    	  if (min_column<minRatio) minRatio = min_column;
    	  if (max_column>MaxRatio) MaxRatio = max_column;
}

setBatchMode(false);
Plot.setLimits(0,nSlices,minRatio,MaxRatio);
Plot.update();



