#include <Stepper.h> // Librairy for stepper motor

// Variable declaration section

int pulsePin = 8;   //define pin for TTL input signal
const int NbPasParTour = 200; // Number of steps for 1 complet rotation (360/1.8=200); the motor is 1.8 stepp 
const int ButtonClockwisepin = 10; //define pin for Clockwise rotation with press button to install the serynge
const int  ButtonCounterClockwisepin=11; //define pin for CounterClockwise rotation with press button to install the serynge
Stepper Moteur1(NbPasParTour, 2, 3, 4, 5);   //define pin for the  motor stepper 



// Setup section:this function executes only once
void setup() { 
  Moteur1.setSpeed(100);  
  Serial.begin(9600); 
  pinMode(pulsePin, OUTPUT);//https://docs.arduino.cc/learn/microcontrollers/digital-pins 
  pinMode(ButtonClockwisepin,OUTPUT);  
  pinMode(ButtonCounterClockwisepin,OUTPUT);.
  } 
  
  void loop() { 
  Serial.println(compteur);
  InstallSeryge();// function to use the serynge pump
 }
  
//function 
void InstallSeryge () {
 
// check if the pushbutton is pressed. If it is, the buttonState is HIGH:
if (digitalRead(ButtonClockwisepin) == HIGH) {
    delay(200); //avoid rebound
    // turn motor in Clockwise:
    Moteur1.step(NbPasParTour/10);
  } 
if (digitalRead(ButtonCounterClockwisepin) == HIGH){
  delay(200);
  // turn motor in Counter Clockwise:
  Moteur1.step(-NbPasParTour/10);
  }
if (digitalRead(pulsePin) == HIGH){
  //delay(50);
  //inject the "drog" by TTL software command
  Moteur1.step(-NbPasParTour);
  }
}

